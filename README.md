# Wardrobify

Team:

* Person 1 - Which microservice?
* Paarth Joshi - Shoes microservice

## Design

## Shoes microservice

The shoe application enables users to effectively manage and monitor their shoe collection. The application's user interface consists of a shoe list and forms for adding new shoes. The back-end programming facilitates the creation of shoe containers and their contents. The application includes forms for inputting shoe details such as the manufacturer, model name, color, picture URL, and bin number, along with the ability to delete shoes. These functionalities are easily accessible through links in the navigation bar. Moreover, the program offers a button that enables users to remove shoes from their collection.

## Hats microservice

I completed a functional react application that manages an inventory of hats. I created two components, HatsList and HatForm, that manages the data. The HatList displays a list of hats that is fetched from the backend api. The details of the list displays each hat with a stle name, fabric, color, picture url, and assigned location. Api requests were also created within the views to manage GET/POST/DELETE requests.
