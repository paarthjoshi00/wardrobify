from django.contrib import admin
from .models import Shoe, BinVO
# Register your models here.

admin.site.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    pass

admin.site.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    pass
