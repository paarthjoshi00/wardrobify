import React, { useEffect, useState } from 'react';

function HatForm() {

    const [formData, setFormData] = useState(
        {
            style_name: '',
            fabric: '',
            color: '',
            picture_url: '',
            location: '',
        }
    )

    const [locations, setLocations] = useState([])

    const fetchLocation = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchLocation();
      }, []);


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8090/api/hats/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                style_name: '',
                fabric: '',
                color: '',
                picture_url: '',
                location: '',
            });
        }
    }


    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Hat</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">

              <div className="form-floating mb-3">
                <input value={formData.style_name} onChange={handleFormChange} placeholder="Style name" required type="text" name="style_name" id="style_name" className="form-control" />
                <label htmlFor="name">Style</label>
              </div>

              <div className="form-floating mb-3">
                <input value={formData.fabric} onChange={handleFormChange} placeholder="Fabric type" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="starts">Fabric</label>
              </div>

              <div className="form-floating mb-3">
                <input value={formData.color} onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="ends">Color</label>
              </div>

              <div className="form-floating mb-3">
                <input value={formData.picture_url} onChange={handleFormChange} placeholder="Picture" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="ends">Picture</label>
              </div>

              <div className="mb-3">
                <select value={formData.location} onChange={handleFormChange} required name="location" id="location" className="form-select">
                  <option value="">Assign location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>{location.closet_name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Add new hat</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default HatForm;