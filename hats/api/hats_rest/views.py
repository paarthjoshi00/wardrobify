from common.json import ModelEncoder
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from hats_rest.models import Hat, LocationVO
import json


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "section_number", "shelf_number"]


class ListHatsEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name", "picture_url", "location", "color", "fabric", "id"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])  # show the list of hats
def list_hats(request):

    if request.method == "GET":
        hats = Hat.objects.all()

        return JsonResponse(
            {"hats": hats},
            encoder=ListHatsEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location_section = content["location"]
            location = LocationVO.objects.get(section_number=location_section)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location section"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def show_hat_detail(request, pk):

    if request.method == "GET":
        hat = Hat.objects.get(id=pk)

        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        hat = Hat.objects.get(id=pk).delete()

        return JsonResponse(
            {"message": "deleted"}
        )
