from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.closet_name


class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(max_length=200, null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hat",
        on_delete=models.PROTECT
    )

    def __str__(self):
        return self.style_name
    
    def get_api_url(self):
        return reverse("show_hat_detail", kwargs={"pk": self.pk})

